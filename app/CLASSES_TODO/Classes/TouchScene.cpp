//
// Created by Magdalena on 2018-04-30.
//

#include <functional>
#include <audio/include/SimpleAudioEngine.h>
#include "TouchScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

/*
 * Stworz obiekty scena(typu Scene) i warstwa(typu TouchScene)
 * Dodaj warstwe do sceny
 * */
Scene* TouchScene::createScene()
{

}

/*
 * Stworz listener typu EventListenerTouchOneByOne, obsluguje pojedyncze stukniecia
 * Przypisz analogicznie jak w komentarzu dla onTouchBegan, onTouchEnded, onTouchMoved, onTouchCancelled
 *
 * Zarejestruj odbieranie eventow
 *
 * Dodaj do sceny labelke z napisem, ktory wyswietli sie przy stuknieciu
 *
 * */
bool TouchScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }

    labelTouchInfo = Label::createWithSystemFont("Dotknij gdziekolwiek by zaczac!", "Arial", 30);


    //touchListener->onTouchBegan = CC_CALLBACK_2(TouchScene::onTouchBegan, this);



    return true;
}

/*
 * Ustaw pozycje napisu, tak aby wylapywala miejsce stukniecia palcem
 * Ustaw jaki napis chcesz pokazywac po kliknieciu
 * */
bool TouchScene::onTouchBegan(Touch* touch, Event* event)
{

    return true;
}

void TouchScene::onTouchEnded(Touch* touch, Event* event)
{
    cocos2d::log("touch ended");
}

void TouchScene::onTouchMoved(Touch* touch, Event* event)
{
    cocos2d::log("touch moved");
}

void TouchScene::onTouchCancelled(Touch* touch, Event* event)
{
    cocos2d::log("touch cancelled");
}

