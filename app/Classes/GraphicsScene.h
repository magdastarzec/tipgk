//
// Created by Magdalena on 2018-04-29.
//

#ifndef PROJ_ANDROID_STUDIO_GRAPHICSSCENE_H
#define PROJ_ANDROID_STUDIO_GRAPHICSSCENE_H

#include "cocos2d.h"
#include <2d/CCLayer.h>

class GraphicsScene : public cocos2d::Layer{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(GraphicsScene);

};


#endif //PROJ_ANDROID_STUDIO_GRAPHICSSCENE_H
