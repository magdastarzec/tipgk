//
// Created by Magdalena on 2018-04-30.
//

#ifndef PROJ_ANDROID_STUDIO_ACTIONSCENE_H
#define PROJ_ANDROID_STUDIO_ACTIONSCENE_H


#include "cocos2d.h"

class ActionScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init() override;
    CREATE_FUNC(ActionScene);

    void update(float) override;

private:
    cocos2d::Sprite* sprite;
};


#endif //PROJ_ANDROID_STUDIO_ACTIONSCENE_H
