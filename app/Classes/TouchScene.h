#ifndef PROJ_ANDROID_STUDIO_TOUCHSCENE_H
#define PROJ_ANDROID_STUDIO_TOUCHSCENE_H
#include "cocos2d.h"

class TouchScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();

    virtual bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*);
    virtual void onTouchEnded(cocos2d::Touch*, cocos2d::Event*);
    virtual void onTouchMoved(cocos2d::Touch*, cocos2d::Event*);
    virtual void onTouchCancelled(cocos2d::Touch*, cocos2d::Event*);
    void onKeyPressed();
    CREATE_FUNC(TouchScene);

private:
    cocos2d::Label* labelTouchInfo;
};


#endif //PROJ_ANDROID_STUDIO_TOUCHSCENE_H
