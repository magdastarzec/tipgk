#include "GraphicsScene.h"
USING_NS_CC;

/*
 * Stworz obiekty scena(typu Scene) i warstwa(typu GraphicsScene)
 * Dodaj warstwe do sceny
 * */
Scene* GraphicsScene::createScene()
{
    auto scene = Scene::create();
    auto layer = GraphicsScene::create();
    scene->addChild(layer);

    return scene;
}

/*
 * 1. Stworz Sprite i jako parametr podaj nazwe obrazka
 * Ustaw pozycje Sprite i dodaj do sceny
 *
 * 2. Dodaj drugi Sprite, ustaw mu pozycję na (0,0) i dodaj go do poprzedniego Sprita - rodzica
 *
 * Ostatecznie powinieneś miec 2 obrazki pizzy na ekranie
 * */
bool GraphicsScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }

    /* Pomocne do pozycjonowania na ekranie */
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    auto sprite = Sprite::create("pizza.png");
    sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));

    auto sprite2 = Sprite::create("pizza2.png");
    sprite2->setPosition(0,0);

    sprite->addChild(sprite2);

    this->addChild(sprite, 0);

    return true;
}