//
// Created by Magdalena on 2018-04-30.
//

#include <functional>
#include <audio/include/SimpleAudioEngine.h>
#include "TouchScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

/*
 * Stworz obiekty scena(typu Scene) i warstwa(typu TouchScene)
 * Dodaj warstwe do sceny
 * */
Scene* TouchScene::createScene()
{
    auto scene = Scene::create();
    auto layer = TouchScene::create();
    scene->addChild(layer);

    return scene;
}

/*
 * Stworz listener typu EventListenerTouchOneByOne, obsluguje pojedyncze stukniecia
 * Przypisz analogicznie jak w komentarzu dla onTouchBegan, onTouchEnded, onTouchMoved, onTouchCancelled
 *
 * Zarejestruj odbieranie eventow
 *
 * Dodaj do sceny labelke z napisem, ktory wyswietli sie przy stuknieciu
 *
 * */
bool TouchScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }

    labelTouchInfo = Label::createWithSystemFont("Dotknij gdziekolwiek by zaczac!", "Arial", 30);

    auto touchListener = EventListenerTouchOneByOne::create();

    //touchListener->onTouchBegan = CC_CALLBACK_2(TouchScene::onTouchBegan, this);
    touchListener->onTouchBegan = CC_CALLBACK_2(TouchScene::onTouchBegan, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(TouchScene::onTouchEnded, this);
    touchListener->onTouchMoved = CC_CALLBACK_2(TouchScene::onTouchMoved, this);
    touchListener->onTouchCancelled = CC_CALLBACK_2(TouchScene::onTouchCancelled, this);

    _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

    this->addChild(labelTouchInfo);

    //efekty dzwiekowe
    auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
    audio->preloadBackgroundMusic("song.mp3");
    audio->playBackgroundMusic("song.mp3");

    return true;
}

/*
 * Ustaw pozycje napisu, tak aby wylapywala miejsce stukniecia palcem
 * Ustaw jaki napis chcesz pokazywac po kliknieciu
 * */
bool TouchScene::onTouchBegan(Touch* touch, Event* event)
{
    labelTouchInfo->setPosition(touch->getLocation());
    labelTouchInfo->setString("PAC!");
    return true;
}

void TouchScene::onTouchEnded(Touch* touch, Event* event)
{
    cocos2d::log("touch ended");
}

void TouchScene::onTouchMoved(Touch* touch, Event* event)
{
    cocos2d::log("touch moved");
}

void TouchScene::onTouchCancelled(Touch* touch, Event* event)
{
    cocos2d::log("touch cancelled");
}

