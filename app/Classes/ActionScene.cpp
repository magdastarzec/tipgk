#include "ActionScene.h"
USING_NS_CC;

/*
 * Stworz obiekty scena(typu Scene) i warstwa(typu ActionScene)
 * Dodaj warstwe do sceny
 * */
Scene* ActionScene::createScene()
{
    auto scene = Scene::create();
    auto layer = ActionScene::create();
    scene->addChild(layer);
    return scene;
}

/*
 * 1. Stworz Sprite i jako parametr podaj nazwe obrazka
 * Ustaw pozycje Sprite i dodaj do sceny.
 *
 * Wywołaj funkcję scheduleUpdate(), ktora wywoluje metode update() danego Node'a
 * */
bool ActionScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    sprite = Sprite::create("gasienica.png");
    sprite->setPosition(this->getBoundingBox().getMidX(), this->getBoundingBox().getMidY());
    this->addChild(sprite, 0);

    this->scheduleUpdate();

    return true;
}

/*
 * update() to nadpisana funkcja odpowiedzialna za poruszanie sie obiektu
 * Stwórz zmienną position i przypisz jej pozycję Sprite (getPosition())
 *
 * Dla wartości x w wektorze position (position.x) przypisz równanie odpowiadające za szybkość porusznia się obrazka.
 *
 * Odkomentuj nową wartość pozycji
 *
 * Ustaw pozycje dla Sprite
 * */
void ActionScene::update(float delta){
    auto position = sprite->getPosition();
    position.x -= 250 * delta;

    // odkomentuj
    if (position.x  < 0 - (sprite->getBoundingBox().size.width / 2))
        position.x = this->getBoundingBox().getMaxX() + sprite->getBoundingBox().size.width/2;

    sprite->setPosition(position);
}